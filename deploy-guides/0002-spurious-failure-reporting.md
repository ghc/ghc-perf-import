# Deploy instructions for spurious failure logging

Follow the standard set by [0001-eventlog-importing.md](0001-eventlog-importing.md).

## SQL for creating tables and privileges

```sql
drop type if exists ci_failure_t cascade;
create type ci_failure_t as enum ('docker');
;
drop table if exists ci_failure;
create table ci_failure (
    failure_id bigserial primary key,
    job_id bigint not null,
    type ci_failure_t,
    job_date timestamp with time zone
);
;
grant insert, select on ci_failure to ghc_perf;
grant usage on ci_failure_failure_id_seq to ghc_perf;
grant select on ci_failure to ghc_perf_web;
```
