{ buildPythonApplication, isPy3k, python-gitlab, falcon, mypy, psycopg2, types-requests }:

buildPythonApplication rec {
  pname = "ghc-perf-import-service";
  version = "0.1";
  src = ./.;
  propagatedBuildInputs = [ python-gitlab falcon psycopg2 ];
  disabled = !isPy3k;
  checkInputs = [ mypy types-requests ];
  checkPhase = ''
    mypy --ignore-missing-imports build
  '';
}
