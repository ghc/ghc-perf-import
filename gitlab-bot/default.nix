let
  pkgs = import (fetchGit {
    url = "https://github.com/NixOS/nixpkgs";
    rev = "dfef2e61107dc19c211ead99a5a61374ad8317f4";
    ref = "release-22.11";
    shallow = true;
  }) {};
in
pkgs.python39Packages.callPackage ./package.nix {}
