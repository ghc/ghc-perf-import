{ gitlab-bot, ghc-perf-import }:
{ lib, config, pkgs, ... }:

let
  serviceUser = "ghc_perf";
in
{
  config = {
    systemd.services.ghc-perf-gitlab-import-bot = {
      description = "ghc-perf metric import bot";
      script = ''
        ghc-perf-import-service \
          --gitlab-root=https://gitlab.haskell.org/ \
          --access-token=${config.services.ghc-perf-import.gitlabToken} \
          --conn-string=postgresql:///ghc_perf \
          --port=7088
      '';
      path = [ pkgs.git gitlab-bot ghc-perf-import ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        User = "${serviceUser}";
        PermissionsStartOnly = true;
        CacheDirectory = "ghc-perf";
      };
    };

    users.users."${serviceUser}" = {
      description = "User for ghc-perf import script";
      isSystemUser = true;
      group = serviceUser;
    };

    users.groups."${serviceUser}" = {};
  };
  
  options = {
    services.ghc-perf-import.gitlabToken = lib.mkOption {
      type = lib.types.str;
      description = "GitLab access token";
    };
  };
}
