{ ghc-perf-import }:
{ pkgs, lib, config, ... }:

let
  dbName = "ghc_perf";
  serviceUser = "ghc_perf";

  #ghc-perf-web = pkgs.callPackage (import ../web.nix) {};

  pre-import-script = ''
    if ! ${pkgs.sudo}/bin/sudo -upostgres ${pkgs.postgresql}/bin/psql -lqtA | grep -q "^${dbName}|"; then
      echo "Initializing schema..."
      ${pkgs.sudo}/bin/sudo -upostgres ${pkgs.postgresql}/bin/psql \
        -c "CREATE ROLE ghc_perf WITH LOGIN;" \
        -c "CREATE DATABASE ${dbName} WITH OWNER ghc_perf;"
      ${pkgs.sudo}/bin/sudo -ughc_perf ${pkgs.postgresql}/bin/psql -U ghc_perf < ${../import/schema.sql}
      ${pkgs.sudo}/bin/sudo -upostgres ${pkgs.postgresql}/bin/psql \
        -c "GRANT SELECT ON ALL TABLES IN SCHEMA public TO ghc_perf_web;"
      echo "done."
    fi
  '';

  import-script = ''
    cd /var/cache/ghc-perf
    if [ ! -d ghc ]; then
      echo "cloning $(pwd)/ghc..."
      git clone https://gitlab.haskell.org/ghc/ghc
    fi
    cd ghc

    echo "updating $(pwd)/ghc..."
    git pull
    git fetch https://gitlab.haskell.org/ghc/ghc-performance-notes.git refs/notes/perf:refs/notes/ci/perf

    echo "importing commits..."
    perf-import-git -c postgresql:///${dbName} -d ghc master

    echo "importing notes..."
    perf-import-notes -c postgresql:///${dbName} -d ghc -R refs/notes/ci/perf
  '';

in {

  imports = [ ../alerts/nixos.nix ];

  config = {
    users.users."${serviceUser}" = {
      description = "User for ghc-perf import script";
      isSystemUser = true;
      group = serviceUser;
    };
    users.groups."${serviceUser}" = {};
  
    systemd.services.ghc-note-perf-import = {
      description = "Update ghc-perf metrics from perf notes";
      preStart = pre-import-script;
      script = import-script;
      path = [ pkgs.git ghc-perf-import ];
      serviceConfig = {
        User = "${serviceUser}";
        PermissionsStartOnly = true;
        CacheDirectory = "ghc-perf";
      };
    };
  
    systemd.timers.ghc-note-perf-import = {
      description = "Periodically update ghc-perf metrics";
      wants = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      timerConfig = {
        OnCalendar = "*-*-* 3:00:00";
        Unit = "ghc-note-perf-import.service";
      };
    };
  };
}

