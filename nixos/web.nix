{ pkgs, lib, config, ... }:

let
  dbName = "ghc_perf";
  dbWebUser = "ghc_perf_web";
  serviceUser = "ghc_perf";
  postgrestUser = "ghc_perf_web";
  postgrestPort = 8889;

  #ghc-perf-web = pkgs.callPackage (import ../web.nix) {};

  postgrestConfig = builtins.toFile "postgrest.conf" ''
    db-uri = "postgres://${dbWebUser}@/${dbName}"
    db-schema = "public"
    db-anon-role = "${dbWebUser}"
    db-pool = 10
  
    server-host = "*4"
    server-port = ${toString postgrestPort}
  '';

in {
  imports = [ ../alerts/nixos.nix ];

  config = {
    users.users."${postgrestUser}" = {
      isSystemUser = true;
      shell = "/bin/false";
      group = postgrestUser;
    };
    users.groups."${postgrestUser}" = {};
  
    systemd.services.postgrest-ghc-perf = {
      description = "Postgrest instance for ghc-perf";
      script = ''
        ${postgrest}/bin/postgrest ${postgrestConfig}
      '';
      wantedBy = [ "multi-user.target" ];
      serviceConfig.User = "${postgrestUser}";
    };
  };
}

