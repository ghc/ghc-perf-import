{
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        packages = rec {
          default = ghc-perf-import;

          ghc-perf-import = pkgs.haskellPackages.callCabal2nix "perf-import" ./import {
            lens-regex-pcre = pkgs.haskellPackages.callHackage "lens-regex-pcre" "0.3.0.0" {};
          };

          gitlab-bot = pkgs.python3Packages.callPackage ./gitlab-bot/package.nix {};
        };
      }
    ) //
    {
      nixosModules.ghc-perf-import = {
        imports = [
          (import ./gitlab-bot/nixos.nix {
            ghc-perf-import = self.packages."x86_64-linux".ghc-perf-import;
            gitlab-bot = self.packages."x86_64-linux".gitlab-bot;
          })
          (import ./nixos/import.nix { inherit (self.packages."x86_64-linux") ghc-perf-import; })
        ];
      };
    };
}
