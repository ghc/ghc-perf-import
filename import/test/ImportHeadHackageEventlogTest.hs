import GHC.RTS.Events
import Test.Hspec
import Test.Hspec.QuickCheck
import Test.QuickCheck

import GhcPerf.Import.HeadHackageEventlog hiding (main)

main :: IO ()
main = hspec $ do
    describe "parseFilename sanity checks" $ do
        it "parses 1" $ do
            parseFilename "mypackage-0.1.0-asdf.eventlog"
                `shouldBe` Just ("mypackage", "0.1.0", "asdf")
        it "parses 2" $ do
            parseFilename "my-special-package-0.1.0-asdf.eventlog"
                `shouldBe` Just ("my-special-package", "0.1.0", "asdf")
        it "doesn't parse" $ do
            parseFilename "any old thing"
                `shouldBe` Nothing
    describe "heapSizeEvent sanity checks" $ do
        prop "parses a HeapSize event" $ do
            \hcap ts sz ecap ->
                heapSizeEvent (Event ts (HeapSize hcap sz) ecap)
                    `shouldBe` Just (HeapEvent ts Megablock sz)
