{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}

module GhcPerf.Import.HeadHackageEventlog where

import Control.Applicative
import Control.Monad
import Control.Exception
import Data.ByteString (ByteString)
import Data.ByteString.Builder (byteString)
import Data.List
import Data.List.NonEmpty (NonEmpty (..), toList)
import Data.List.Split
import Data.Maybe
import Data.Word
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.SqlQQ
import Database.PostgreSQL.Simple.ToField
import GHC.RTS.Events
import System.Directory
import System.Environment
import System.Exit
import System.FilePath
import System.IO
import System.IO.Error
import qualified Data.ByteString.Char8 as BS

-- | Entrypoint. Check args.
main = do
    as <- getArgs
    progName <- getProgName
    case as of
        [logDir, commitSha, jobId, connString] ->
            importLogDir logDir commitSha jobId (BS.pack connString)
        _ -> die $ "Usage: " <> progName <> " EVENTLOG_DIR COMMIT_SHA JOB_ID CONN_STRING"

-- | Real function.
--
-- NOTE: Wraps the *entire* import run in a single DB transaction. That means
-- this utility performs disk i/o within that transaction. Not a problem for
-- version 0.0.1, but maybe a problem eventually.
importLogDir
    :: FilePath -- ^ Where eventlogs are
    -> String -- ^ GHC commit being processed
    -> String -- ^ Gitlab job id
    -> ByteString -- ^ Postgresql connection string
    -> IO ()
importLogDir dir sha jobId connString = do
    files <- logFiles dir
    conn <- connectPostgreSQL connString

    withTransaction conn $ do
        c <- fetchCommit conn sha
        importId <- insertImport conn jobId c
        mapM_ (importFile conn importId) files

-- | Register metadata about this import.
insertImport conn jobId commitId = do
    res <-
        query conn [sql|
            insert into eventlog_import (commit_id, gitlab_job_id, date)
            values (?, ?, NOW())
            returning import_id
        |]
        (commitId, jobId)
    maybe
        (die $ "Import insertion failure")
        (pure . fromOnly)
        (listToMaybe res)

-- | Process one eventlog file / package
importFile conn importId filepath = do
    dat <- packageDat filepath
    maybe empty (insertPackage conn importId) dat

-- | Insert the extracted package data
insertPackage conn importId dat = do
    packageId <- insertPackageMeta conn dat
    insertEvents conn importId packageId (packageEvs dat)

-- | Look up the database ID for a given commit.
--
-- If the same sha already exists, reuse it.
fetchCommit :: Connection -> String -> IO Int
fetchCommit conn sha = do
    _ <- execute conn
        [sql|
            insert into commits (commit_sha)
            values (?)
            on conflict do nothing
        |]
        (Only sha)
    res <- query conn
        [sql|
            select commit_id from commits where commit_sha = ?
        |]
        (Only sha)
    maybe
        (die $ "Could not find commit: " ++ sha)
        (pure . fromOnly)
        (listToMaybe res)

-- | Insert the package metadata
--
-- If the same (name, ver, sha) already exists, reuse it.
insertPackageMeta :: Connection -> PackageDat -> IO Int
insertPackageMeta conn dat = do
    let params = (packageName dat, packageVer dat, packageSha dat)
    _ <- execute conn
        [sql|
            insert into package (name, ver, sha)
            values (?, ?, ?)
            on conflict do nothing
        |]
        params
    res <- query conn
        [sql|
            select package_id
            from package
            where name = ?
            and ver = ?
            and sha = ?
        |]
        params
    maybe
        (die $ "Package insertion failure: " ++ show res)
        (pure . fromOnly)
        (listToMaybe res)

-- | Insert the heap events
insertEvents
    :: Connection
    -> Int -- ^ id for this import job's metadata
    -> Int -- ^ id for the package being processed
    -> NonEmpty HeapEvent
    -> IO ()
insertEvents conn inputId packageId
    = void
    . executeMany conn
        [sql|
            insert into heap_event
                (import_id, package_id, timestamp, measure, value)
            values
            (?, ?, ?, ?, ?)
        |]
    . map (\(HeapEvent ts meas val) -> (inputId, packageId, ts, meas, val))
    . toList

-- | Extract metadata and heap events
packageDat fp = case parseFilename fp of
    Nothing -> die $ "Unexpected shape of filename: " ++ fp
    Just (package, ver, sha) -> do
        e <- readEventLogFromFile fp
        either die `flip` e $ \ log  ->
            let evs = events (dat log)
                heapEvs = mapMaybe heapSizeEvent evs
            in case heapEvs of
                [] -> empty
                (d:ds) -> pure $ Just $ PackageDat package ver sha (d:|ds)

-- | The GHC events we care about
data HeapMeasure
    = Megablock -- ^ HEAP_SIZE
    | Block     -- ^ BLOCKS_SIZE
    | Live      -- ^ HEAP_LIVE
    deriving (Eq, Show)

instance ToField HeapMeasure where
    toField Megablock = Plain (byteString "'megablock'::heap_measure")
    toField Block = Plain (byteString "'block'::heap_measure")
    toField Live = Plain (byteString "'live'::heap_measure")

-- | Relevant data about the heap events
data HeapEvent = HeapEvent Timestamp HeapMeasure Word64
    deriving (Eq, Show)


-- All of a package's data (metadata and heap events)
data PackageDat = PackageDat
    { packageName :: String
    , packageVer :: String
    , packageSha :: String
    , packageEvs :: NonEmpty HeapEvent
    } deriving (Eq, Show)

-- | Parse a HeapEvent from 'Event'
heapSizeEvent :: Event -> Maybe HeapEvent
heapSizeEvent ev = case evSpec ev of
    HeapSize{ sizeBytes = sz }    -> Just (HeapEvent (evTime ev) Megablock sz)
    BlocksSize{ blocksSize = sz } -> Just (HeapEvent (evTime ev) Block sz)
    HeapLive{ liveBytes = sz }    -> Just (HeapEvent (evTime ev) Live sz)
    _ -> Nothing

-- | Breaks foo-1.0-<long-sha> into ("foo", "1.0", "<long-sha>")
parseFilename :: FilePath -> Maybe (String, String, String)
parseFilename fp =
    let parts = reverse $ splitOn "-" (takeBaseName fp)
    in case parts of
        (sha:version:rest)
            | not (null rest)
            -> Just (intercalate "-" (reverse rest), version, sha)
        _ -> Nothing

-- | Just reads the eventlog directory and collects filepaths
logFiles logDir = do
    ls <- tryJust (guard . isDoesNotExistError) (listDirectory logDir)
    case ls of
        Left _ -> missingIsOk
        Right [] -> missingIsOk
        Right logs@(_:_) -> pure (map (combine logDir) logs)
    where
        missingIsOk = do
            progName <- getProgName
            hPutStrLn stderr $
                progName <> ": Log dir '" <> logDir <> "' is missing or empty, so there is nothing to do."
            exitSuccess
