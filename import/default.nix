{ pkgs }:
let
  config = {
    packageOverrides = pkgs: {
      haskellPackages = pkgs.haskellPackages.override {
        overrides = self: super: {
          ghc-events = self.callHackageDirect {
            pkg = "ghc-events";
            ver = "0.17.0.3";
            sha256 = "sha256-SfyCSzc6YMpcFL2wOaaWeTM9xdHpt05Hdh0GzTT7G54=";
          } {};
        };
      };
    };
  };

  drv = pkgs.haskellPackages.callCabal2nix "perf-import" ./. { };

in

  if pkgs.lib.inNixShell then drv.env else drv
